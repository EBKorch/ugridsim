# Micro-grid simulation GUI

Simple implementation of a preconfigured micro-grid system.

Under the hood the project uses the [sim-framework](https://gitlab.com/EBKorch/sim-framework)
library for the simulation, and [simgui-blocks](https://gitlab.com/EBKorch/simgui-blocks)
and [gtkscene-rs](https://gitlab.com/EBKorch/gtkscene-rs) for the GUI building.

## Building

The project uses clean Cargo toolchain, so building can be done with the `cargo`
command.

To get the toolchain, install [Rustup](https://rustup.rs/), and follow the
instructions (defaults are good).

The GUI uses the GTK3 toolkit, this must be available on every platform:

*OpenSuse*

```
zypper install gtk3-devel
```

*Fedora*

```
dnf install gtk3-devel
```

*Ubuntu*

```
apt install libgtk-3-dev
```

*Windows*

For windows you first have to install the following softwares:

- [Build Tools for Visual Studio](https://visualstudio.microsoft.com/downloads/#build-tools-for-visual-studio-2019)

- [MSYS2](https://www.msys2.org/)

- [Rustup](https://rustup.rs/)

Install the required packages from MSYS2 terminal:

```
pacman -S mingw-w64-x86_64-gtk3 mingw-w64-x86_64-pkg-config mingw-w64-x86_64-gcc
```

To be able to use the installed packages, make sure that `C:\msys64\mingw64\bin`
is the first in the PATH environment variable.

When using `cargo` to build the project we have to use the GNU toolchain.
Install it with `rustup`:

```
rustup target add x86_64-pc-windows-gnu
```

Before the project could be built we have to set the following environment
variables to allow GNU cross-compilation and set path to GTK libraries:

- PKG_CONFIG_ALLOW_CROSS=1

- GTK_LIB_DIR="C:\msys64\mingw64\lib"

After that we can use `cargo` from the project directory to run the
application:

```
cargo run --release --target=x86_64-pc-windows-gnu
```

In short (assuming PowerShell):

```
$env:PATH="C:\msys64\mingw64\bin;"+$env:PATH
$env:PKG_CONFIG_ALLOW_CROSS=1
$env:GTK_LIB_DIR="C:\msys64\mingw64\lib"
cargo run --release --target=x86_64-pc-windows-gnu
```

To create a distributable archive:

```
cd $PROJECT\target\x86_64-pc-windows-gnu\release
mkdir release
cp *.exe release\
cp C:\msys64\mingw64\bin\*.dll release\
mkdir release\share\glib-2.0\schemas
cp C:\msys64\mingw64\share\glib-2.0\schemas\gschemas.compiled release\share\glib-2.0\schemas\gschemas.compiled
cp -r C:\msys64\mingw64\share\icons release\share\icons
cp -r C:\msys64\mingw64\lib\gdk-pixbuf-2.0 release\lib\gdk-pixbuf-2.0
```

## Running

To build and run the project, run the following command in the project root:

```
cargo run --release
```