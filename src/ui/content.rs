extern crate gtk;
use gtk::prelude::*;

pub struct Content {
    pub sidebar: gtk::Paned,
    pub sidebar_panel: gtk::Box,
    pub area: gtk::DrawingArea,
    pub inspector: gtk::Box,
    pub islanded_switch: gtk::Switch,
}

impl Content {
    pub fn new(builder: &gtk::Builder) -> Self {
        let sidebar: gtk::Paned = get_object!(builder, "sidebar");
        let sidebar_panel: gtk::Box = get_object!(builder, "sidebar_panel");
        let area: gtk::DrawingArea = get_object!(builder, "drawing_area");
        let inspector: gtk::Box = get_object!(builder, "inspector_box");
        let islanded_switch: gtk::Switch = get_object!(builder, "islanded_switch");

        Content { sidebar, sidebar_panel, area, inspector, islanded_switch }
    }
}