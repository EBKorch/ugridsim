use std::{
    rc::Rc,
    cell::RefCell,
};
use gtk::prelude::*;
use units::{
    pixel::PixelPoint,
    traits::InMeters,
};

pub struct Header {
    pub headerbar: gtk::HeaderBar,
    pub show_sidebar_button: gtk::ToggleButton,
    pub viewinfo: Rc<RefCell<ViewInfo>>,
    pub control_buttons: ControlButtons,
}

impl Header {
    pub fn new(builder: &gtk::Builder) -> Self {
        let show_sidebar_button: gtk::ToggleButton = get_object!(builder, "sidebar_togglebutton");
        let headerbar: gtk::HeaderBar = get_object!(builder, "headerbar");
        let control_buttons = ControlButtons::new(builder);
        let viewinfo = Rc::new(RefCell::new(ViewInfo::new(builder)));

        Header { show_sidebar_button, headerbar, viewinfo, control_buttons }
    }
}

pub struct ControlButtons {
    pub run: gtk::ToggleButton,
    pub pause: gtk::ToggleButton,
    pub reset: gtk::Button,
}

impl ControlButtons {
    pub fn new(builder: &gtk::Builder) -> Self {
        let run: gtk::ToggleButton = get_object!(builder, "start_button");
        let pause: gtk::ToggleButton = get_object!(builder, "pause_button");
        let reset: gtk::Button = get_object!(builder, "reset_button");

        ControlButtons { run, pause, reset }
    }
}

pub struct ViewInfo {
    pub offset: gtk::Label,
    pub scale: gtk::Label,
}

impl ViewInfo {
    pub fn new(builder: &gtk::Builder) -> Self {
        let offset: gtk::Label = builder.get_object("offset_label")
            .expect("Cannot find 'offset_label' in glade file");
        let scale: gtk::Label = builder.get_object("scale_label")
            .expect("Cannot find 'scale_label' in glade file");
        ViewInfo { offset, scale }
    }

    pub fn set(&mut self, scale: &f64, offset: &PixelPoint) {
        self.scale.set_text(&format!("{:.1} px/m", scale));
        let x = offset.x.clone().in_meters(scale, offset);
        let y = offset.y.clone().in_meters(scale, offset);
        let x_sign = if x.0 < 0.0 { "" } else { "+" };
        let y_sign = if y.0 < 0.0 { "" } else { "+" };
        self.offset.set_text(&format!("{}{:.3},{}{:.3} m", x_sign, x.0, y_sign, y.0));
    }
}