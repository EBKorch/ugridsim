use std::sync::{ Arc, Mutex, RwLock };
use rand::Rng;
use ugrid::Bus;
use std::collections::HashMap;
use std::time;
use std::f64::consts::PI;

use std::fs::File;

use cairo::{
    ImageSurface,
    Format,
    Context };

use gtkscene::{
    scene::Scene,
    appearance::Appearance,
    object::{ Object, PixelObject },
};
use sim_framework::{
    simulation::{ Simulation, MeasError },
    simtrait::SimItem,
    measurement::Property,
    dowcast_as_any_ref,
};
use units::{
    metric::{ Meters, Point },
    pixel::{ Pixels, PixelPoint },
    traits::InPixels,
};

use crate::ugrid::{self, Interface};
use crate::ugrid::items::*;
use crate::ugrid::unit::*;

use simgui_blocks::Inspector;

const UPDATE_INTERVAL_MS: u32 = 16;

pub struct UIState {
    pub simulation: Arc<Mutex<Simulation>>,
    variants: Arc<RwLock<HashMap<String, ItemVariant>>>,
    timer: Option<glib::SourceId>,
    drawtimer: Option<glib::SourceId>,
    inspector: Arc<Mutex<Option<Inspector>>>,
    inspected: Arc<Mutex<Option<String>>>,
    pub scene: Arc<Mutex<Scene>>,
    pub bus: Arc<Mutex<Bus>>,
}

impl UIState {
    pub fn new(drawing_area: &gtk::DrawingArea)-> Self {
        let mut simulation = Simulation::new();
        simulation.set_history_time(10.0);
        simulation.set_cleanup_frequency(100);
        let (mut incoming_bus, items, measure) = ugrid::example_config();
        let bus = Arc::new(Mutex::new(incoming_bus));
        let bus_clone = Arc::clone(&bus);
        simulation.set_postproc(Box::new(move |simitems| {
            let mut bus = bus_clone.lock().unwrap();
            bus.postproc(simitems);
            println!("Remaining balance: {:.3}", bus.balance/1_000.0)
        }));
        let variants = Arc::new(RwLock::new(HashMap::new()));
        //let mut rng = rand::thread_rng();
        let mut state = UIState {
            simulation: Arc::new(Mutex::new(simulation)),
            timer: None,
            drawtimer: None,
            scene: Arc::new(Mutex::new(Scene::new(drawing_area.clone()))),
            variants,
            inspector: Arc::new(Mutex::new(None)),
            inspected: Arc::new(Mutex::new(None)),
            bus,
        };
        for (name,itemVariant, item) in items {
            state.add_item(&name, itemVariant, item);
        }
        for (name, property) in measure {
            state.measure(&name, &property, 0.0)
                .expect(&format!("Cannot find property '{}' on item '{}'", property, name));
        }
        state
    }

    pub fn add_item(&mut self, name: &str, variant: ItemVariant, item: Box<dyn SimItem>) {
        // Before we hand over the data extract the orientation
        let orientation = item.orientation();
        // Insert the item into the simulation
        self.simulation.lock()
            .expect("Cannot get lock on simulation")
            .insert(name, item);
        // Store which variant is it. This is used for downcasting and drawing
        self.variants.write()
            .expect(&format!("Cannot get lock on State.variants while adding '{}'", name))
            .insert(name.to_string(), variant.clone());
        // Update the view as well
        if let Some(ori) = orientation {
            self.scene.lock().unwrap().insert(name, Object {
                pos: Point::new(ori.x, ori.y),
                rot: ori.rot,
                appear: variant.to_appearance(),
            });
        }
    }

    pub fn measure(&mut self, name: &str, property: &str, samplingtime: f64) -> Result<(), MeasError> {
        self.simulation.lock()
            .expect("Cannot get a lock on simulation to set measurement")
            .measure(name, property, samplingtime)
    }

    pub fn start_simulation(&mut self) {
        // If simulation is already running, do nothing
        if self.timer.is_some() { return }
        // Create clones from common data to be able to pass them between
        // threads
        let simulation = Arc::clone(&self.simulation);
        let scene = Arc::clone(&self.scene);
        let objects = Arc::new(Mutex::new(HashMap::new()));
        let objects_copy = Arc::clone(&objects);
        let inspected_item = Arc::new(Mutex::new(None));
        let inspected_item_update = Arc::clone(&inspected_item);
        let inspected = Arc::clone(&self.inspected);
        let variants = Arc::clone(&self.variants);
        // Create a new glib (async) timer. This will call the closure in
        // periodic times
        self.timer = Some(glib::timeout_add(UPDATE_INTERVAL_MS, move || {
            // Get a lock on simulation to be able to use it - if the return is
            // ok make one simulation step
            match simulation.lock() {
                Ok(mut sim) => {
                    // Start a time to measure the simulation time
                    let start = time::Instant::now();
                    // Step the simulation - this is entirely handled by the
                    // `Simulation` structure
                    sim.step(UPDATE_INTERVAL_MS as f64);
                    // Create new object from simulation items. Objects will be
                    // used for displaying.
                    let mut new_objects = HashMap::with_capacity(sim.item_count());
                    {
                        // Get a lock on variants - this holds the item types,
                        // so we can decide which appearance to use.
                        let variants_ro = variants.read()
                            .expect("Cannot get lock on State.variants");
                        // Request the name-orientation pairs from the
                        // simulation
                        for (name, orientation) in sim.orientations() {
                            let appear = match variants_ro.get(name) {
                                Some(variant) => variant.to_appearance(),
                                None => {
                                    // Use a default appearance
                                    Appearance::Agent
                                }
                            };
                            // For every name-value pair we add a new object
                            // with its location and appearance
                            new_objects.insert(name.to_owned(), Object {
                                pos: Point::new(orientation.x, orientation.y),
                                rot: orientation.rot,
                                appear
                            });
                        }
                    }
                    // Get a lock on the shared object list, and set the new
                    // data. This way the drawing will only halt, when we update
                    // the data
                    *objects_copy.lock().unwrap() = new_objects;
                    if let Some(name) = &*inspected.lock().unwrap() {
                        *inspected_item.lock().unwrap() = Some(sim.get_item(&name).unwrap().borrow().clone_box());
                    }
                    // Print the elapsed time
                    println!("Simulation delta: {} ms", start.elapsed().as_millis());
                }
                Err(_) => panic!("Mutex guarding the simulation is poisoned"),
            }
            glib::Continue(true)
        }));

        // Make a clone of shared data
        let inspector = Arc::clone(&self.inspector);
        let inspected = Arc::clone(&self.inspected);
        let simulation = Arc::clone(&self.simulation);
        // Start the drawing timer which will update the scene with new object
        // data
        self.drawtimer = Some(glib::timeout_add_local(UPDATE_INTERVAL_MS, move || {
            // Get a lock on the shared objects
            let mut locked_objects = objects.lock().unwrap();
            // If there is no item in it, there is nothing to do
            if locked_objects.len() < 1 { return glib::Continue(true) }
            // Get a lock on the scene to be able to update it
            match scene.lock() {
                Ok(scene) => {
                    // Efficiently replace the old object data with the new one
                    scene.objects.replace(std::mem::replace(&mut *locked_objects, HashMap::new()));
                    scene.update();
                    // Update the inspector as well - if we have any
                    if inspected.lock().unwrap().is_some() {
                        if let Some(item) = &*inspected_item_update.lock().unwrap() {
                            if let Some(inspector) = &mut *inspector.lock().unwrap() {
                                inspector.update( &*simulation.lock().unwrap(), item);
                            }
                        }
                    }
                }
                Err(e) => panic!("Periodic scene update failed: {}", e),
            }
            glib::Continue(true)
        }));
    }

    pub fn stop_simulation(&mut self) {
        if self.timer.is_none() { return }
        
        let id = std::mem::replace(&mut self.timer, None).unwrap();
        glib::source_remove(id);
    }

    pub fn inspect<'b>(&'b self, name: &str) -> Vec<gtk::Widget> {
        *self.inspected.lock().unwrap() = Some(name.to_string());

        let mut rows = vec![];

        let variant_ro = self.variants.read().unwrap();
        let variant = variant_ro.get(name)
            .expect(&format!("Cannot find variant for '{}'", name));
        let simulation = self.simulation.lock()
            .expect("Cannot get a lock on the simulation");
        let simitem = simulation.get_item(&name.to_string())
            .expect(&format!("Cannot find simulated item for '{}'", name));

        match variant {
            ItemVariant::Consumer => {
                let item = simitem.borrow().clone_box();
                let consumer = dowcast_as_any_ref!(item, Consumer);
                let mut inspector = Inspector::new();
                // Name
                inspector.new_section(
                    "Name",
                    "Unique identifier of the grid element"
                );
                inspector.add_static_row("ID / Name", name);
                inspector.close_section();

                // Plots
                let name = name.to_string();
                // Power plot
                let name_clone = name.clone();
                inspector.new_section("Plots", "Time dependent variable values are displayed here");
                inspector.add_live_plot(
                    &*simulation,
                    &item,
                    Box::new(move |sim, _| {
                        sim.obtain_measurement(&name, "Power")
                            .expect(&format!("{}", name)).frames_by_elapsed_time(5.0).into_iter().cloned().collect()
                }));
                inspector.add_live_plot(
                    &*simulation,
                    &item,
                    Box::new(move |sim, _| {
                        sim.obtain_measurement(&name_clone , "Frequency")
                            .expect(&format!("{}", name_clone)).frames_by_elapsed_time(5.0).into_iter().cloned().collect()
                    }));
                inspector.close_section();

                rows = inspector.rows.clone();
                *self.inspector.lock().unwrap() = Some(inspector);
            }
            ItemVariant::Producer => {
                let item = simitem.borrow().clone_box();
                let producer = dowcast_as_any_ref!(item, Producer);
                let mut inspector = Inspector::new();
                // Name
                inspector.new_section(
                    "Name",
                    "Unique identifier of the grid element"
                );
                inspector.add_static_row("ID / Name", name);
                inspector.close_section();

                // Plots
                let name = name.to_string();
                // Power plot
                let name_clone = name.clone();
                inspector.new_section("Plots", "Time dependent variable values are displayed here");
                inspector.add_live_plot(
                    &*simulation,
                    &item,
                    Box::new(move |sim, _| {
                        sim.obtain_measurement(&name, "Power")
                            .expect(&format!("{}", name)).frames_by_elapsed_time(5.0).into_iter().cloned().collect()
                    }));
                inspector.add_live_plot(
                    &*simulation,
                    &item,
                    Box::new(move |sim, _| {
                        sim.obtain_measurement(&name_clone, "Frequency")
                            .expect(&format!("{}", name_clone)).frames_by_elapsed_time(5.0).into_iter().cloned().collect()
                    }));
                inspector.close_section();

                rows = inspector.rows.clone();
                *self.inspector.lock().unwrap() = Some(inspector);
            }
            ItemVariant::Storage => {
                let item = simitem.borrow().clone_box();
                let storage = dowcast_as_any_ref!(item, Storage);
                let mut inspector = Inspector::new();
                // Name
                inspector.new_section(
                    "Name",
                    "Unique identifier of the grid element"
                );
                inspector.add_static_row("ID / Name", name);
                inspector.close_section();

                // Plots
                let name = name.to_string();
                // Power plot
                let name_clone = name.clone();
                inspector.new_section("Plots", "Time dependent variable values are displayed here");
                inspector.add_live_plot(
                    &*simulation,
                    &item,
                    Box::new(move |sim, _| {
                        sim.obtain_measurement(&name, "Level")
                            .expect(&format!("{}", name)).frames_by_elapsed_time(5.0).into_iter().cloned().collect()
                    }));
                inspector.add_live_plot(
                    &*simulation,
                    &item,
                    Box::new(move |sim, _| {
                        sim.obtain_measurement(&name_clone, "Frequency")
                            .expect(&format!("{}", name_clone)).frames_by_elapsed_time(5.0).into_iter().cloned().collect()
                    }));
                inspector.close_section();

                rows = inspector.rows.clone();
                *self.inspector.lock().unwrap() = Some(inspector);
            }
            ItemVariant::Wind => {
                let item = simitem.borrow().clone_box();
                let storage = dowcast_as_any_ref!(item, Storage);
                let mut inspector = Inspector::new();
                // Name
                inspector.new_section(
                    "Name",
                    "Unique identifier of the grid element"
                );
                inspector.add_static_row("ID / Name", name);
                inspector.close_section();

                // Plots
                let name = name.to_string();
                // Power plot
                let name_clone = name.clone();
                inspector.new_section("Plots", "Time dependent variable values are displayed here");
                inspector.add_live_plot(
                    &*simulation,
                    &item,
                    Box::new(move |sim, _| {
                        sim.obtain_measurement(&name, "Power")
                            .expect(&format!("{}", name)).frames_by_elapsed_time(5.0).into_iter().cloned().collect()
                    }));
                inspector.close_section();

                rows = inspector.rows.clone();
                *self.inspector.lock().unwrap() = Some(inspector);
            }
            ItemVariant::Wind => ()
        }

        // Return
        rows
    }
}

#[derive(Clone)]
pub enum ItemVariant {
    Consumer,
    Producer,
    Storage,
    Wind,
}

impl ItemVariant {
    pub fn to_appearance(&self) -> Appearance {
        match self {
            ItemVariant::Consumer => Appearance::Custom(|cr, pixelobject, scale| {
                let width = Meters(0.15).in_pixels(scale, &PixelPoint{ x: Pixels(0.0), y: Pixels(0.0) });
                let half_width = width.0/2.0;
                let third_width = width.0/3.0;
                cr.set_line_width(1.0);
                cr.move_to(pixelobject.pos.x.0-half_width, pixelobject.pos.y.0-half_width);
                cr.line_to(pixelobject.pos.x.0-half_width, pixelobject.pos.y.0+half_width);
                cr.line_to(pixelobject.pos.x.0+half_width, pixelobject.pos.y.0+half_width);
                cr.line_to(pixelobject.pos.x.0+half_width, pixelobject.pos.y.0-half_width);
                cr.line_to(pixelobject.pos.x.0-half_width, pixelobject.pos.y.0-half_width);
                cr.stroke();
                cr.set_line_width(5.0);
                cr.move_to(pixelobject.pos.x.0-half_width, pixelobject.pos.y.0);
                cr.line_to(pixelobject.pos.x.0-width.0, pixelobject.pos.y.0);
                cr.move_to(pixelobject.pos.x.0-width.0, pixelobject.pos.y.0-1.5*half_width);
                cr.line_to(pixelobject.pos.x.0-width.0, pixelobject.pos.y.0+1.5*half_width);
                cr.stroke();
                cr.set_line_width(2.0);
                cr.move_to(pixelobject.pos.x.0-third_width, pixelobject.pos.y.0);
                cr.line_to(pixelobject.pos.x.0-third_width, pixelobject.pos.y.0+third_width);
                cr.line_to(pixelobject.pos.x.0+third_width, pixelobject.pos.y.0+third_width);
                cr.line_to(pixelobject.pos.x.0+third_width, pixelobject.pos.y.0);
                cr.line_to(pixelobject.pos.x.0-third_width, pixelobject.pos.y.0);
                cr.line_to(pixelobject.pos.x.0, pixelobject.pos.y.0-third_width);
                cr.line_to(pixelobject.pos.x.0+third_width, pixelobject.pos.y.0);
                cr.stroke();
            }),
            ItemVariant::Producer => Appearance::Custom(|cr, pixelobject, scale| {
                let width = Meters(0.15).in_pixels(scale, &PixelPoint{ x: Pixels(0.0), y: Pixels(0.0) });
                let half_width = width.0/2.0;
                let third_width = width.0/3.0;
                cr.set_line_width(1.0);
                cr.move_to(pixelobject.pos.x.0-half_width, pixelobject.pos.y.0-half_width);
                cr.line_to(pixelobject.pos.x.0-half_width, pixelobject.pos.y.0+half_width);
                cr.line_to(pixelobject.pos.x.0+half_width, pixelobject.pos.y.0+half_width);
                cr.line_to(pixelobject.pos.x.0+half_width, pixelobject.pos.y.0-half_width);
                cr.line_to(pixelobject.pos.x.0-half_width, pixelobject.pos.y.0-half_width);
                cr.stroke();
                cr.set_line_width(5.0);
                cr.move_to(pixelobject.pos.x.0+half_width, pixelobject.pos.y.0);
                cr.line_to(pixelobject.pos.x.0+width.0, pixelobject.pos.y.0);
                cr.move_to(pixelobject.pos.x.0+width.0, pixelobject.pos.y.0-1.5*half_width);
                cr.line_to(pixelobject.pos.x.0+width.0, pixelobject.pos.y.0+1.5*half_width);
                cr.stroke();
                cr.set_line_width(3.5);
                cr.move_to(pixelobject.pos.x.0, pixelobject.pos.y.0+third_width);
                cr.line_to(pixelobject.pos.x.0, pixelobject.pos.y.0-0.5*third_width);
                cr.stroke();
                cr.set_line_width(2.0);
                cr.move_to(pixelobject.pos.x.0, pixelobject.pos.y.0+third_width);
                cr.line_to(pixelobject.pos.x.0, pixelobject.pos.y.0-0.5*third_width);
                cr.line_to(pixelobject.pos.x.0-third_width, pixelobject.pos.y.0-0.5*third_width);
                cr.move_to(pixelobject.pos.x.0, pixelobject.pos.y.0-0.5*third_width);
                cr.line_to(pixelobject.pos.x.0+0.5*third_width, pixelobject.pos.y.0-0.5*third_width+0.87*third_width);
                cr.move_to(pixelobject.pos.x.0, pixelobject.pos.y.0-0.5*third_width);
                cr.line_to(pixelobject.pos.x.0+0.5*third_width, pixelobject.pos.y.0-0.5*third_width-0.87*third_width);
                cr.stroke();
            }),
            ItemVariant::Storage => Appearance::Custom(|cr, pixelobject, scale| {
                let width = Meters(0.15).in_pixels(scale, &PixelPoint{ x: Pixels(0.0), y: Pixels(0.0) });
                let half_width = width.0/2.0;
                let third_width = width.0/3.0;
                cr.set_line_width(1.0);
                cr.move_to(pixelobject.pos.x.0-half_width, pixelobject.pos.y.0-half_width);
                cr.line_to(pixelobject.pos.x.0-half_width, pixelobject.pos.y.0+half_width);
                cr.line_to(pixelobject.pos.x.0+half_width, pixelobject.pos.y.0+half_width);
                cr.line_to(pixelobject.pos.x.0+half_width, pixelobject.pos.y.0-half_width);
                cr.line_to(pixelobject.pos.x.0-half_width, pixelobject.pos.y.0-half_width);
                cr.stroke();
                cr.set_line_width(5.0);
                cr.move_to(pixelobject.pos.x.0+half_width, pixelobject.pos.y.0);
                cr.line_to(pixelobject.pos.x.0+width.0, pixelobject.pos.y.0);
                cr.move_to(pixelobject.pos.x.0+width.0, pixelobject.pos.y.0-1.5*half_width);
                cr.line_to(pixelobject.pos.x.0+width.0, pixelobject.pos.y.0+1.5*half_width);
                cr.stroke();
                cr.set_line_width(2.0);
                cr.move_to(pixelobject.pos.x.0-third_width, pixelobject.pos.y.0);
                cr.line_to(pixelobject.pos.x.0-third_width, pixelobject.pos.y.0+0.75*third_width);
                cr.line_to(pixelobject.pos.x.0+third_width, pixelobject.pos.y.0+0.75*third_width);
                cr.line_to(pixelobject.pos.x.0+third_width, pixelobject.pos.y.0);
                cr.line_to(pixelobject.pos.x.0+1.2*third_width, pixelobject.pos.y.0);
                cr.line_to(pixelobject.pos.x.0+1.2*third_width, pixelobject.pos.y.0-third_width);
                cr.line_to(pixelobject.pos.x.0-1.2*third_width, pixelobject.pos.y.0-third_width);
                cr.line_to(pixelobject.pos.x.0-1.2*third_width, pixelobject.pos.y.0);
                cr.line_to(pixelobject.pos.x.0-third_width, pixelobject.pos.y.0);
                cr.move_to(pixelobject.pos.x.0-third_width, pixelobject.pos.y.0-0.5*third_width);
                cr.line_to(pixelobject.pos.x.0-0.5*third_width, pixelobject.pos.y.0-0.5*third_width);
                cr.move_to(pixelobject.pos.x.0+third_width, pixelobject.pos.y.0-0.5*third_width);
                cr.line_to(pixelobject.pos.x.0+0.5*third_width, pixelobject.pos.y.0-0.5*third_width);
                cr.move_to(pixelobject.pos.x.0+0.75*third_width, pixelobject.pos.y.0-0.25*third_width);
                cr.line_to(pixelobject.pos.x.0+0.75*third_width, pixelobject.pos.y.0-0.75*third_width);
                cr.stroke();
            }),
            ItemVariant::Wind => Appearance::Custom(|cr, pixelobject, scale| {
                let width = Meters(0.01).in_pixels(scale, &PixelPoint{ x: Pixels(0.0), y: Pixels(0.0) });
                let half_width = width.0/2.0;
                cr.move_to(pixelobject.pos.x.0-half_width, pixelobject.pos.y.0-half_width);
                cr.line_to(pixelobject.pos.x.0-half_width, pixelobject.pos.y.0+half_width);
                cr.line_to(pixelobject.pos.x.0+half_width, pixelobject.pos.y.0+half_width);
                cr.line_to(pixelobject.pos.x.0+half_width, pixelobject.pos.y.0-half_width);
                cr.line_to(pixelobject.pos.x.0-half_width, pixelobject.pos.y.0-half_width);
                let third_width = width.0/3.0;
                cr.line_to(pixelobject.pos.x.0, pixelobject.pos.y.0-half_width-third_width);
                cr.line_to(pixelobject.pos.x.0+half_width, pixelobject.pos.y.0-half_width);
                cr.stroke();
            }),
        }
    }
}