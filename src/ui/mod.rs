extern crate gtk;
use gtk::prelude::*;

use std::sync::{ Arc, Mutex };

#[macro_export]
macro_rules! get_object {
    ( $builder:tt, $name:tt ) => {
        $builder.get_object($name).expect(&format!("Cannot find '{}' in ui file)", $name))
    }
}

//mod inspector;
mod content;
use content::Content;
mod header;
use header::Header;
mod state;
pub use state::ItemVariant;

use state::UIState;

pub struct Application {
    window: gtk::ApplicationWindow,
    header: Header,
    content: Content,
    state: Arc<Mutex<UIState>>,
}

impl Application {
    pub fn new(app: &gtk::Application, builder: &gtk::Builder) -> Self {
        //let window: gtk::ApplicationWindow = builder.get_object("application_window")
        //    .expect("Cannot find main window in glade file");
        let window: gtk::ApplicationWindow = get_object!(builder, "application_window");
        window.set_application(Some(app));

        let header = Header::new(builder);
        let content = Content::new(builder);

        let state = Arc::new(Mutex::new(UIState::new(&content.area)));

        Application { window, header, content, state }
    }

    pub fn connect(&mut self) {

        // Hide the side panel when header bar button clicked
        let sidebar_panel = self.content.sidebar_panel.clone();
        self.header.show_sidebar_button.connect_clicked(move |_| {
            if sidebar_panel.get_property("visible").unwrap().get().unwrap().unwrap() {
                sidebar_panel.hide();
            } else {
                sidebar_panel.show_all();
            }
        });

        // Update viewport information in headerbar when drawing area is transformed
        let viewinfo = self.header.viewinfo.clone();
        self.state.lock().unwrap().scene.lock().unwrap().set_transform_fn(move |scale, offset| {
            viewinfo.borrow_mut().set(&scale, offset);
        });

        // Update inspector when clicking on an object in the scene
        let inspector = self.content.inspector.clone();
        let state = Arc::clone(&self.state);
        self.state.lock().unwrap().scene.lock().unwrap().set_item_click_fn(move |name| {
            for child in inspector.get_children() { inspector.remove(&child) }
            //let content = simulation.lock().unwrap().get_item(name).unwrap().inspect(name);
            for child in state.lock().unwrap().inspect(name) { inspector.pack_start(&child, true, false, 0) }
            inspector.show_all();
        });

        // Start the simulation when the start button is pushed
        let pause_button = self.header.control_buttons.pause.clone();
        let state = self.state.clone();
        self.header.control_buttons.run.connect_toggled(move |w| {
            if w.get_active() {
                pause_button.set_active(false);
                state.lock().unwrap().start_simulation();
            } else {
                pause_button.set_active(true);
                state.lock().unwrap().stop_simulation();
            }
        });
        // Stop the simulation with the pause button
        let run_button = self.header.control_buttons.run.clone();
        let state = self.state.clone();
        self.header.control_buttons.pause.connect_clicked(move |w| {
            if w.get_active() {
                run_button.set_active(false);
                state.lock().unwrap().stop_simulation();
            } else {
                run_button.set_active(true);
                state.lock().unwrap().start_simulation();
            }
        });
        // Clear the simulation with the reset button
        let state = self.state.clone();
        let run_button = self.header.control_buttons.run.clone();
        let pause_button = self.header.control_buttons.pause.clone();
        self.header.control_buttons.reset.connect_clicked(move |_| {
            state.lock().unwrap().stop_simulation();
            run_button.set_active(false);
            pause_button.set_active(true);
        });
        // Toggle islanded mode of the bus
        let bus = Arc::clone(&self.state.lock().unwrap().bus);
        self.content.islanded_switch.connect_state_set(move |_, value| {
            let mut bus = bus.lock().unwrap();
            bus.islanded = value;
            gtk::Inhibit(false)
        });
    }

    pub fn show_all(&self) {
        self.window.show_all()
    }
}