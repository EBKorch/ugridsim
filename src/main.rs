extern crate gtk;
extern crate gio;
extern crate glib;

use gio::prelude::*;

use std::env::args;

#[macro_use]
extern crate sim_framework;

#[macro_use]
mod ui;
mod ugrid;

fn build_app(app: &gtk::Application) {
    // Load the ui from a ui XML file
    let builder = gtk::Builder::from_string(include_str!(concat!(env!("CARGO_MANIFEST_DIR"), "/resources/main.ui")));

    // The actual UI building is implemented in as a separate structure. Pass
    // the previously created builder, and will build up the UI
    let mut application = ui::Application::new(app, &builder);
    // Connecting sets the callbacks for UI elements (buttons, checkboxes, etc)
    application.connect();
    // Display every widget after setting up is finished
    application.show_all();
}

fn main() {
    // Create a new gtk application - this will throw an error if the GTK
    // context cannot be created.
    let application = gtk::Application::new(
        Some("com.dda.agent-simulation"),
        Default::default()
    ).expect("Failed to initialize application"); 

    // Build the UI of the app
    application.connect_activate(|app| {
        build_app(app);
    });

    // Start the application with the command line arguments
    application.run(&args().collect::<Vec<_>>());
}
