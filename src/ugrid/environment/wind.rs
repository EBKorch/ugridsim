use sim_framework::simtrait::{SimItem, World, PropertyMap, Orientation};

use std::f64::consts::PI;
use rand::prelude::*;
use std::collections::HashMap;
use sim_framework::measurement::Property;
use units::metric::Meters;

/// Defines a simulateable variable-speed wind effect
///
/// The current speed of the wind is given with a simple formula:
///
/// cos('elapsed time' * 'frequency') * 'factor' + 'random component'
///
/// The resulting wind speed therefore will have a harmonic base which is
/// derogated by a random component.
///
/// The aim of this simplistic approach is to get an input for windmills, and
/// investigate a complete system behavior under varying windmill based energy
/// producers.
#[derive(Clone)]
pub struct Wind {
    current: f64,
    average: f64,
    delta: f64,
    uncertainty: f64,
    frequency: f64,
    accum_time: f64,
}

impl Wind {
    /// Creates a new instance of this structure
    ///
    /// Every parameter used for the wind speed simulation can be given here:
    ///
    /// - *average*: Average value of the wind speed. The current speed will
    /// oscillate around this value.
    /// - *delta*: Size of the maximum deterministic oscillation. This controls
    /// the maximum value of the sinusoidal component.
    /// - *frequency*: Frequency of the sinusoidal component
    /// - *uncertainty*: Random component of the current wind speed.
    ///
    /// The returned `Wind` instance will have an initial speed value, which
    /// will be update in every simulation step.
    pub fn new(average: f64, delta: f64, frequency: f64, uncertainty: f64) -> Self {
        let mut wind = Wind {
            current: 0.0,
            average,
            delta,
            frequency,
            uncertainty,
            accum_time: 0.0
        };
        wind.current = wind.calc_speed();
        wind
    }

    /// Returns with the current wind speed
    ///
    /// The current wind speed value of the wind cannot be accessed directly to
    /// to protect it from being overwritten. We only want the `Wind` instance
    /// itself be able to change that.
    pub fn current_speed(&self) -> &f64 {
        &self.current
    }

    // Internal funtion used to calculate the current wind speed from the base
    // parameters
    fn calc_speed(&self) -> f64 {
        self.average + (self.accum_time*self.frequency).cos()*self.delta + (random::<f64>()-0.5)*2.0*self.uncertainty
    }

    // To be able to calculate the time-dependent component correctly we need to
    // accumulate the time value. In this function we also reset the accumulated
    // value when a full round (2*PI) is completed.
    fn accum_time(&mut self, delta_sec: f64) {
        self.accum_time += delta_sec;
        // If the time value if over 2*PI we remove a full round (2*PI) from it
        if self.accum_time*self.frequency >= 2.0*PI {
            self.accum_time -= 2.0*PI/self.frequency;
        }
    }
}

// We implement `SimItem` on the `Wind` as we eant to be able to use it in
// the simulation module.
impl SimItem for Wind {
    fn sim(&mut self, _delta_ms: f64, _world: &mut World) {
        // Convert time to seconds and append to accumulated time
        let seconds = _delta_ms/1000.0;
        self.accum_time(seconds);
        // Calculate current speed value
        self.current = self.calc_speed();
        // This is just a debugging message
        println!("Current wind speed: {:.2} km/h", self.current);
    }

    fn properties(&self) -> PropertyMap {
        // Create the initially empty property map
        let mut map: PropertyMap = HashMap::default();
        // Add power - currently this is the only property we will be able to
        // measure and display on the GUI
        map.insert("Power".to_string(), Box::new(|simitem| {
            let wind: &Wind = dowcast_as_any_ref!(simitem, Wind);
            Property::Float(wind.current)
        }));
        // Return with the populated map
        map
    }
}