use sim_framework::simtrait::SimItem;
use units::metric::Meters;

use super::unit::{
    Current,
    Watt,
};

pub trait Interface: SimItem {
    fn balance(&self) -> Watt;
    fn current(&self) -> Current;
    fn pushpull_energy(&mut self, energy: &Watt) -> Watt {
        energy.clone()
    }
    fn update_freq(&mut self, freq: f64);
    fn set_position(&mut self, x: Meters, y: Meters) {}
}