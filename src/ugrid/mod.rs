mod bus;
pub use bus::Bus;
mod traits;
use sim_framework::simtrait::SimItem;
use items::GridItem;

pub use traits::Interface;
pub mod unit;
pub mod items;
pub mod environment;

use crate::ui::ItemVariant;

type NamedItem = (String, ItemVariant, Box<dyn SimItem>);

pub fn example_config() -> (Bus, Vec<NamedItem>, Vec<(String, String)>) {
    // Create a list with measured name-property pairs
    let mut measure = vec![];
    // Create the initially empty bus
    let mut bus = Bus::default();
    // Define items and add them to the bus
    let mut storage = items::Storage::new(
        200_000.0,
        unit::Volt(230.0),
        unit::CurrentType::AC,
        unit::Frequency::new(
            50.0,
            0.0,
        )
    );
    let storage_name = "Battery";
    bus.add(storage_name, GridItem::Storage);
    measure.push((storage_name.to_string(), "Level".to_string()));
    measure.push((storage_name.to_string(), "Frequency".to_string()));


    let mut producer = items::Producer::new(
        6_000.0,
        unit::Volt(230.0),
        unit::CurrentType::AC,
        10.0,
        unit::Frequency::new(
            50.0,
            -1_000.0,
        )
    );
    let producer_name = "Windmill";
    bus.add(producer_name, GridItem::Producer);
    measure.push((producer_name.to_string(), "Power".to_string()));
    measure.push((producer_name.to_string(), "Frequency".to_string()));


    let mut consumer = items::Consumer::new(
        5_500.0,
        unit::Volt(230.0),
        unit::CurrentType::AC,
        unit::Frequency::new(
            50.0,
            0.0,
        )
    );
    let consumer_name = "Residental home";
    bus.add(consumer_name, GridItem::Consumer);
    measure.push((consumer_name.to_string(), "Power".to_string()));
    measure.push((consumer_name.to_string(), "Frequency".to_string()));


    // Define environment
    let wind = environment::Wind::new(
        9.6,
        1.2,
        0.3,
        3.0,
    );
    let wind_name = "Wind";
    measure.push((wind_name.to_string(), "Power".to_string()));
    // Collect items
    {
        let interface_items: Vec<(&str, &mut dyn Interface)> = vec![
            (consumer_name, &mut consumer),
            (producer_name, &mut producer),
            (storage_name, &mut storage),
        ];
        bus.arrange(interface_items);
    }
    // Create the list of items
    let items: Vec<NamedItem> = vec![
        (storage_name.to_string(),ItemVariant::Storage, Box::new(storage)),
        (producer_name.to_string(), ItemVariant::Producer, Box::new(producer)),
        (consumer_name.to_string(),ItemVariant::Consumer, Box::new(consumer)),
        (wind_name.to_string(),ItemVariant::Wind, Box::new(wind)),
    ];
    // Return with the bus and the items
    (bus, items, measure)
}