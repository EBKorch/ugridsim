use sim_framework::{
    simtrait::{SimItem, World},
    simulation::Items,
    dowcast_as_any_mut, dowcast_as_any_ref,
};
use units::metric::Meters;

use super::{traits::Interface, unit::Watt};
use super::items::{GridItem, Consumer, Storage, Producer};

#[derive(Clone)]
pub struct Bus {
    items: Vec<(String, GridItem)>,
    pub balance: Watt,
    pub islanded: bool,
    pub freq: f64,
    pos_update: bool,
}

impl Bus {
    pub fn add(&mut self, name: &str, grid_item: GridItem) {
        self.items.push((name.to_owned(), grid_item));
        self.pos_update = true;
    }
    pub fn get_grid_item(& self, search_name: &str) -> Option<&GridItem>{
        for (name, gridItem) in &self.items{
            if name == search_name {
                return Some(gridItem);
            }
        }
        None
    }

    pub fn arrange(&self, items: Vec<(&str, &mut dyn Interface)>) {
        let mut left_count = 0.0;
        let mut right_count = 0.0;
        for (name, item) in items {
            let variant = self.items.iter().find_map(|(n, variant)| if n == name {
                Some(variant)
            } else {
                None
            }).expect(&format!("Cannot find '{}' amoung bus items", name));
            let (x_pos, y_pos) = match variant {
                GridItem::Consumer => {
                    right_count = right_count+1.0;
                    (Meters(0.5), Meters(right_count*0.20))
                },
                GridItem::Storage => {
                    left_count = left_count+1.0;
                    (Meters(0.2), Meters(left_count*0.20))
                },
                GridItem::Producer => {
                    left_count = left_count+1.0;
                    (Meters(0.2), Meters(left_count*0.20))
                }
            };
            item.set_position(x_pos, y_pos);
        }
    }

    pub fn postproc(&mut self, items: &mut Items) {
        self.balance = 0.0;
        let mut factor = 0.0;
        let arrange_items: Vec<(&str, &mut dyn Interface)> = vec![];
        for (name, item_variant) in &self.items {
            let item = items.iter().find_map(|(n, data)| if n == name {
                Some(data)
            } else {
                None
            }).expect(&format!("Item '{}' is part of the bus, but cannot find it in the simulation", name));
            // Calculate item balance, and update position of requested
            let mut item = item.borrow_mut();
            let (value, interface) = match item_variant {
                GridItem::Consumer => {
                    let consumer: &mut Consumer = dowcast_as_any_mut!(item, Consumer);
                    let consumer: &mut dyn Interface = consumer;
                    (consumer.balance(), consumer)
                }
                GridItem::Storage => {
                    let storage: &mut Storage = dowcast_as_any_mut!(item, Storage);
                    let storage: &mut dyn Interface = storage;
                    (0.0, storage)
                },
                GridItem::Producer => {
                    let producer: &mut Producer = dowcast_as_any_mut!(item, Producer);
                    let producer: &mut dyn Interface = producer;
                    (producer.balance(), producer)
                }
            };
            self.balance = self.balance.clone() + value;
            factor = factor.clone() + match item_variant {
                GridItem::Consumer => 0.0,
                GridItem::Storage => 0.0,
                GridItem::Producer => {
                    let producer: &Producer = dowcast_as_any_mut!(item, Producer);
                    producer.current().freq.factor
                }
            };
        }
        // If position wa update, we no longer need it to be true
        if self.pos_update {
            self.arrange(arrange_items);
            self.pos_update = false;
        }

        if self.balance != 0.0 {
            for (name, grid_item) in &self.items {
                let item = items.iter().find_map(|(n, data)| if n == name {
                    Some(data)
                } else {
                    None
                }).expect(&format!("Item '{}' is part of the bus, but cannot find it in the simulation", name));
                self.balance = match grid_item {
                    GridItem::Consumer => self.balance,
                    GridItem::Storage => {
                        let mut item = item.borrow_mut();
                        let item: &mut Storage = dowcast_as_any_mut!(item, Storage);
                        println!("Level: {:.3} kW; Requested: {:.3} kW", item.level()/1_000.0, self.balance/1_000.0);
                        item.pushpull_energy(&self.balance)
                    }
                    GridItem::Producer => self.balance,
                };
                if self.balance == 0.0 { break }
            }
        }

        // ToDo: calculate bus frequency
        if self.islanded && factor != 0.0  {
            print!("Running in islanded mode,  ");
            self.freq = self.freq - self.balance/factor;
        } else {
            print!("Running in connected mode, ");
            self.freq = 50.0;
        };
        println!("frequency: {:.2}", self.freq);
        // Update items with common frequency
        for (name, grid_item) in &self.items {
            let item = items.iter().find_map(|(n, data)| if n == name {
                Some(data)
            } else {
                None
            }).expect(&format!("Item '{}' is part of the bus, but cannot find it in the simulation", name));
            let mut item = item.borrow_mut();
            let interface: &mut dyn Interface = match grid_item {
                GridItem::Consumer => {
                    dowcast_as_any_mut!(item, Consumer)
                },
                GridItem::Storage => {
                    dowcast_as_any_mut!(item, Storage)
                }
                GridItem::Producer => {
                    dowcast_as_any_mut!(item, Producer)
                },
            };
            interface.update_freq(self.freq);
        }
    }
}

impl Default for Bus {
    fn default() -> Self {
        let items = vec![];
        let balance = 0.0;
        let islanded = false;
        let freq = 50.0;
        let pos_update = false;
        Bus { items, balance, islanded, freq, pos_update }
    }
}

impl SimItem for Bus {}

#[derive(Clone)]
pub struct Connection {
    to: String,
    priority: usize,
}

impl Connection {
    pub fn new(to: &str, priority: usize) -> Self {
        Connection { to: to.to_owned() , priority }
    }
}