use crate::ugrid::{
    unit::*,
    traits::Interface,
};
use sim_framework::simtrait::{SimItem, Orientation, PropertyMap};
use units::metric::Meters;
use std::collections::HashMap;
use sim_framework::measurement::Property;

#[derive(Clone)]
pub struct Storage {
    pub capacity: Watt,
    level: Watt,
    current: Current,
    pub position: (Meters, Meters),
}

impl Storage {
    pub fn new(capacity: Watt, voltage: Volt, acdc: CurrentType, freq: Frequency) -> Self {
        Storage {
            capacity,
            current: Current::new(voltage, acdc, freq),
            level: 0.0,
            position: ( Meters(0.0), Meters(0.0) ),
        }
    }

    pub fn level(&self) -> &Watt {
        &self.level
    }
}

impl SimItem for Storage {

    fn orientation(&self) -> Option<Orientation> {
        Some(Orientation { x: self.position.0.clone(), y: self.position.1.clone(), rot: 0.0 })
    }

    fn properties(&self) -> PropertyMap {
        let mut map: PropertyMap = HashMap::default();
        // Add power
        map.insert("Level".to_string(), Box::new(|simitem| {
            let storage: &Storage = dowcast_as_any_ref!(simitem, Storage);
            Property::Float(storage.level)
        }));
        map.insert("Frequency".to_string(), Box::new(|simitem| {
            let storage: &Storage = dowcast_as_any_ref!(simitem, Storage);
            Property::Float(storage.current.freq.current)
        }));
        map
    }
}

impl Interface for Storage {
    fn balance(&self) -> Watt {
        0.0
    }

    fn current(&self) -> Current {
        self.current.clone()
    }

    fn pushpull_energy(&mut self, energy: &Watt) -> Watt {
        if *energy > 0.0 {
            if energy + self.level > self.capacity {
                let ret = energy - (self.capacity - self.level);
                self.level = self.capacity;
                ret
            } else {
                self.level += energy;
                0.0
            }
        } else {
            if -energy > self.level {
                let ret = energy + self.level;
                self.level = 0.0;
                ret
            } else {
                self.level += energy;
                0.0
            }
        }
    }

    fn update_freq(&mut self, freq: f64) {
        self.current.freq.current = freq;
    }

    fn set_position(&mut self, x: Meters, y: Meters) {
        self.position = (x, y);
    }
}