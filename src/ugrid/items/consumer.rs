use std::collections::HashMap;

use crate::ugrid::{
    unit::*,
    traits::Interface,
};
use sim_framework::simtrait::{ SimItem, World, Orientation, PropertyMap };
use sim_framework::measurement::Property;
use units::metric::Meters;

/// Simulates a simple energy consumer inside a microgrid
///
/// This item has a static power usage, and can be feed to satistfy every need
/// of it.
#[derive(Clone)]
pub struct Consumer {
    pub power: Watt,
    balance: Watt,
    current: Current,
    pub position: (Meters, Meters),
}

impl Consumer {
    /// Creates a new `Consumer`
    ///
    /// Base properties can be assigned here. Currently only the power property
    /// is relevant which defines the power requiremento of the consumer.
    ///
    /// One can also set the voltage level and the AC/DC type of the item, but
    /// currently it is not used in the simulation.
    pub fn new(power: Watt, voltage: Volt, acdc: CurrentType, freq: Frequency) -> Self {
        Consumer {
            power: power,
            current: Current::new(voltage, acdc, freq),
            balance: 0.0,
            position: ( Meters(0.0), Meters(0.0) ),
        }
    }
}

// We implement the `SimItem` type on the `Consumer` to be able to pass it to
// the simulation framework
impl SimItem for Consumer {
    // We set the required power in every round to the nominal requirement
    fn sim(&mut self, _delta_ms: f64, _world: &mut World) {
        self.balance = -self.power.clone();
    }

    // With orientation we can control where the item, on the GUI will be
    // displayed. In this function we only create an `Orientation` structure
    // from the position value - which is controlled by the bus when the
    // consumer is connected to one.
    fn orientation(&self) -> Option<Orientation> {
        Some(Orientation { x: self.position.0.clone(), y: self.position.1.clone(), rot: 0.0 })
    }

    // Export some properties for the simulation framework - properties listed
    // here can be measured and displayed on the GUI.
    fn properties(&self) -> PropertyMap {
        // Create the initially empty property map.
        let mut map: PropertyMap = HashMap::default();
        // Add power
        map.insert("Power".to_string(), Box::new(|simitem| {
            let consumer: &Consumer = dowcast_as_any_ref!(simitem, Consumer);
            Property::Float(consumer.power)
        }));
        // Export its frequency as well
        map.insert("Frequency".to_string(), Box::new(|simitem| {
            let consumer: &Consumer = dowcast_as_any_ref!(simitem, Consumer);
            Property::Float(consumer.current().freq.current)
        }));
        // Return with the populated property map
        map
    }
}

// We want to be able to add the `Consumer` to the `Bus`, so the `Interface`
// trai must be implemented on it.
impl Interface for Consumer {
    fn balance(&self) -> Watt {
        self.balance.clone()
    }

    fn current(&self) -> Current {
        self.current.clone()
    }

    // The `Consumer` can only recieve energy, so only react to the
    // pushed/pulled power value if its above zero (so we give energy to it).
    fn pushpull_energy(&mut self, energy: &Watt) -> Watt {
        if energy > &0.0 {
            // If we try to give more energy, than required, satisfy the balance
            // and return with the remaining energy.
            if *energy > self.balance {
                let ret = *energy - self.balance;
                self.balance = 0.0;
                ret
            } else {
                // If we require more energy than passed, consumer everything,
                // and return with nothing
                self.balance += energy;
                0.0
            }
        } else {
            energy.clone()
        }
    }

    fn update_freq(&mut self, freq: f64) {
        self.current.freq.current = freq;
    }

    fn set_position(&mut self, x: Meters, y: Meters) {
        self.position = (x, y);
    }
}