use sim_framework::simtrait::SimItem;

mod consumer;
pub use consumer::Consumer;
mod storage;
pub use storage::Storage;
mod producer;
pub use producer::Producer;

#[derive(Clone)]
pub enum GridItem {
    Consumer,
    Storage,
    Producer,
}
