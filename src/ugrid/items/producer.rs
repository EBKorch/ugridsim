use crate::ugrid::{environment::Wind, traits::Interface, unit::*};
use sim_framework::simtrait::{SimItem, World, Orientation, PropertyMap};
use units::metric::Meters;
use std::collections::HashMap;
use sim_framework::measurement::Property;

/// Represents a simple windmill implementation
///
/// The windmill has a nominal wind speed requirement with which it produces its
/// maximum power. If the wind is lower the porduced power will linearly
/// decrease.
///
/// Therefore for this windmill implementation the two most important value is
/// the rated wind speed and rated power.
#[derive(Clone)]
pub struct Producer {
    power: Watt,
    maximum_wind: f64,
    pub balance: Watt,
    current: Current,
    pub position: (Meters, Meters),
}

impl Producer {
    /// Create a new windmill
    ///
    /// Currently only the rated power and maximum wind speed is used, the
    /// voltage and AC/DC type is only there for future implementation work.
    pub fn new(power: Watt, voltage: Volt, acdc: CurrentType, maximum_wind: f64, freq: Frequency) -> Self {
        Producer {
            power,
            balance: 0.0,
            current: Current::new(voltage, acdc, freq),
            maximum_wind,
            position: ( Meters(0.0), Meters(0.0) ),
        }
    }
}

// Implement `SimItem` on `Producer` to be able to use it in the simulation
// module
impl SimItem for Producer {
    // In every simulation step we try to find the wind item with the name
    // "Wind". If we cannot retrieve an item with such a name the produced
    // power will be zero, as there is no wind.
    //
    // If the wind is found, and its speed is equal or higher than the rated
    // wind speed of the windmill it will porduce its rated power.
    //
    // However, if the wind speed is lower, the produced power will be linearly
    // decreased.
    fn sim(&mut self, _delta_ms: f64, _world: &mut World) {
        // Calculate the maximum power considering the frequency drop
        let power = self.power + self.current.freq.factor*(self.current.freq.current - 50.0);
        if let Some(wind) = _world.iter().find_map(|(name, data)| if name == "Wind" {
            Some(data)
        } else {
            None
        }) {
            let wind = dowcast_as_any_ref!(wind, Wind);
            let current_speed = wind.current_speed();
            if current_speed > &self.maximum_wind {
                self.balance = power;
            } else if current_speed <= &0.0 {
                self.balance = 0.0;
            } else {
                self.balance = power * (current_speed/&self.maximum_wind)
            }
        } else {
            self.balance = power;
        }
        println!("Producer provides {:.3} kW", self.balance/1_000.0);
    }

    fn orientation(&self) -> Option<Orientation> {
        Some(Orientation { x: self.position.0.clone(), y: self.position.1.clone(), rot: 0.0 })
    }

    fn properties(&self) -> PropertyMap {
        let mut map: PropertyMap = HashMap::default();
        // Add power
        map.insert("Power".to_string(), Box::new(|simitem| {
            let producer: &Producer = dowcast_as_any_ref!(simitem, Producer);
            Property::Float(producer.power)
        }));
        map.insert("Frequency".to_string(), Box::new(|simitem| {
            let producer: &Producer = dowcast_as_any_ref!(simitem, Producer);
            Property::Float(producer.current().freq.current)
        }));
        map
    }
}

impl Interface for Producer {
    fn balance(&self) -> Watt {
        self.balance.clone()
    }

    fn current(&self) -> Current {
        self.current.clone()
    }

    fn pushpull_energy(&mut self, energy: &Watt) -> Watt {
        if *energy > 0.0 {
            energy.clone()
        } else {
            if -energy > self.balance {
                let ret = energy + self.balance;
                self.balance = 0.0;
                ret
            } else {
                self.balance += energy;
                0.0
            }
        }
    }

    fn update_freq(&mut self, freq: f64) {
        self.current.freq.current = freq;
    }

    fn set_position(&mut self, x: Meters, y: Meters) {
        self.position = (x, y);
    }
}