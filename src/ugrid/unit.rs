use std::ops::{Add, Sub, Neg};


pub type Watt = f64;
#[derive(Clone)]
pub struct Volt(pub f64);

#[derive(Clone)]
pub struct Ampere(pub f64);

#[derive(Clone)]
pub struct Current {
    current: CurrentType,
    voltage: Volt,
    pub freq: Frequency,
}

impl Current {
    pub fn new(voltage: Volt, current: CurrentType, freq: Frequency) -> Self {
        Current { current, voltage, freq }
    }

    pub fn new_ac(voltage: Volt, freq: Frequency) -> Self {
        Current::new(voltage, CurrentType::AC, freq)
    }

    pub fn new_dc(voltage: Volt, freq: Frequency) -> Self {
        Current::new(voltage, CurrentType::DC, freq)
    }
}

#[derive(Clone)]
pub enum CurrentType {
    AC,
    DC,
}

#[derive(Clone)]
pub struct Frequency {
    pub nominal: f64,
    pub factor: f64,
    pub current: f64,
}

impl Frequency {
    pub fn new(nominal: f64, factor: f64) -> Self {
        Frequency {
            current: nominal,
            nominal,
            factor,
        }
    }
}
